/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

/**
 *
 * @author Zhel-PC
 */
public class BloqueTexto extends BloqueAbstracto {
    String texto; //variable de clase
    
    
    BloqueTexto(String texto) { //parametros del constructor
        this.texto = texto;
    }
    
    public String getTexto() {
        return texto;
    }
    
    @Override
    public String getContenido() {
        return texto;
    }
}