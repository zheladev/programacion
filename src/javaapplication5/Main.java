/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

/**
 *
 * @author Zhel-PC
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BloqueAbstracto bloqueUno = new BloqueTexto("Bloque texto 1");
        BloqueAbstracto bloqueDos = new BloqueImagen("as8d12d==", "Bloque texto 2");
        BloqueAbstracto bloqueTres = new BloqueNuevo("bloque nuevo");
        
        BloqueAbstracto[] documento = new BloqueAbstracto[3];
        documento[0] = bloqueUno;
        documento[1] = bloqueDos;
        documento[2] = bloqueTres;
        
        for(int i = 0; i < documento.length; i++) {
            String indice = documento[i].getContenido();
            System.out.println(indice);
        }
    }
    
}
