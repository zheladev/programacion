/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

/**
 *
 * @author Zhel-PC
 */
public class BloqueImagen extends BloqueTexto {
    String b64_image; //d82hjf891j==12d81=d211n19192sj12
    
    BloqueImagen(String b64, String texto) {
        super(texto);
        b64_image = b64;
    }
    
    public String getImage() {
        return b64_image;
    }
    
        @Override
    public String getContenido() {
        return b64_image + "\t" + texto;
    }
}
