/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

/**
 *
 * @author Zhel-PC
 */
public class java {
    public static void main(String[] args) {
        int numero = 10;
        int[] numeroRef = {10};
        
        //paso por valor, se copia el valor que se le pasa a la funcion, y aunque se cambie dentro de ella, fuera se mantiene
        int numeroSumado = sumarDiez(numero);
        System.out.println("numero despues de llamar a sumarDiez(): " + numero);
        
        //paso por referencia, se le pasa a la funcion la direccion en memoria en la que esta guardado el objeto, si modificamos dentro de la funcion
        //dicho objeto, fuera tambien se cambiara
        sumarDiezReferencia(numeroRef);
        System.out.println("numero despues de llamar a sumarDiezReferencia(): " + numeroRef[0]);
    }
    
    public static void sumarDiezReferencia(int[] array) {
        array[0] = array[0]+ 10;
        System.out.println("sumarDiezReferencia(): " + array[0]);
    }
    
    public static int sumarDiez(int n) {
        n = n + 10;
        System.out.println("sumarDiez(): " + n);
        return n;
    }
}
