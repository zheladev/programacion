/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

/**
 *
 * @author Zhel-PC
 */
public class recursion {
    public static void main(String[] args) {
           int numeroFibo = 5;
           for (int i = 0; i < 10; i++) {
               System.out.println(fiboRec(i));
           }
    }
    
    public static int fiboRec(int n) {
        if (n < 2) {
            return n;
        } else {
            return fiboRec(n-1) + fiboRec(n-2);
        }
    }
    
    public static int getFibonacci(int n){
        if (n == 0) {
            return 0;
        }
        
        if (n == 1){
            return 1;
        }

        int first = 0;
        int second = 1;
        int nth = 1;

        for (int i = 2; i <= n; i++) {
            nth = first + second;
            first = second;
            second = nth;
        }
        return nth;
    }
}
